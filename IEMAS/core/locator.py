import logging
from pyage.core.agent.agent import AGENT
from pyage.core.inject import Inject
from math import ceil, sqrt
import random


class Locator(object):
    def get_neighbour(self, agent):
        raise NotImplementedError()


class GridParentLocator(Locator):

    def get_neighbour(self, agent):
        siblings = list(agent.parent.get_agents())
        if len(siblings) < 2:
            return None
        index = siblings.index(agent)
        size = len(siblings)
        dim = int(ceil(sqrt(size)))
        return siblings[random.choice([index - dim, index - 1, index + 1, index + dim]) % size]