import logging
from pyage.core.statistics import Statistics
import time
import math
import numpy

logger = logging.getLogger(__name__)


class AgentsStatistics(Statistics):
    def __init__(self, agents2, maturity, output_file_name='fitness_pyage.txt'):
        self.history = []
        self.fitness_output = open('output.txt', 'a')
        self.agents = agents2
        self.maturity = maturity
        self.start_time = time.clock()

    def __del__(self):
        self.fitness_output.close()

    def append(self, best_fitness, step_count, l, dead, fitness_calls, bites, mature, cumulativeTime, moment_of_inertia):
        self.fitness_output.write(
            str(step_count - 1) + '\t' + str(abs(best_fitness)) + '\t' + str(l) + "\t" + str(dead) + '\t' + str(fitness_calls) + '\t' +str(
                mature) + '\t' + str(bites) + '\t' + str(cumulativeTime) + '\t' + str(moment_of_inertia) + '\n')

    def update(self, step_count, agents):
        try:
            best_fitness = max(a.get_fitness() for a in agents)
            logger.info(best_fitness)
            self.history.append(best_fitness)
            logger.info(max(len(a.get_agents()) for a in agents))
            i = 0
            bites = 0
            mature = 0
            fitness_calls = 0
            for a in agents:
                for b in a.get_agents():
                    fitness_calls += b.fitness_calls
                    if b.dead:
                        i += 1
                        bites += b.bites
                        if b.steps >= self.maturity:
                            mature += 1
            moment_of_inertia = self.moment_of_inertia(agents)
            logger.info("number of dead: " + str(i) + " mature: " + str(mature) + " number of bites: " + str(bites) + " moment of inertia: " + str(moment_of_inertia))
            #if step_count % 10 == 0:
            self.append(best_fitness, step_count, max(len(a.get_agents()) for a in agents) - i, fitness_calls, i,  bites, mature, 1000*(time.clock() - self.start_time), moment_of_inertia)
            self.start_time = time.clock()
        except:
            logging.exception("")

    def summarize(self, agents):
        try:
            logger.debug(self.history)
            logger.debug("best genotype: %s", max(agents, key=lambda a: a.get_fitness).get_best_genotype())
        except:
            logging.exception("")

    def moment_of_inertia(self, agents):
        genotype_size = 50
        moment_of_inertia = 0
        alive_population = 0
        c = numpy.zeros(genotype_size)

        for a in agents:
            for agent in a.get_agents():
                if agent.dead is False:
                    alive_population += 1
                    for i in range(len(agent.genotype.genes)):
                        c[i] += agent.genotype.genes[i]
        for i in range(len(c)):
            c[i] = c[i]/alive_population
        for a in agents:
            for agent in a.get_agents():
                if agent.dead is False:
                    for i in range(len(agent.genotype.genes)):
                        inertia = math.pow(agent.genotype.genes[i] - c[i], 2)
                        moment_of_inertia += inertia
        return (genotype_size * moment_of_inertia)/alive_population
