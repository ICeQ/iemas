
from random import uniform
from pyage.solutions.evolution.genotype import FloatGenotype
from IEMAS.core.iemas import VampireAgent

def float_iemas_initializer(dims=2,energy=100, size=100, lowerbound=0.0, upperbound=1.0):
    agents = {}
    for i in range(size):
        agent = VampireAgent(FloatGenotype([uniform(lowerbound, upperbound) for _ in range(dims)]), energy)
        agents[agent.get_address()] = agent
    return agents
