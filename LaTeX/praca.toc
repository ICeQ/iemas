\select@language {polish}
\contentsline {chapter}{\numberline {1}Wprowadzenie}{7}
\contentsline {chapter}{\numberline {2}Algorytmy ewolucyjne jako technika optymalizacji}{9}
\contentsline {section}{\numberline {2.1}Optymalizacja}{9}
\contentsline {subsection}{\numberline {2.1.1}Definicja optymalizacji}{9}
\contentsline {subsection}{\numberline {2.1.2}Metody optymalizacji}{9}
\contentsline {section}{\numberline {2.2}Algorytmy ewolucyjne}{11}
\contentsline {subsection}{\numberline {2.2.1}Inspiracje biologiczne}{11}
\contentsline {subsection}{\numberline {2.2.2}Przebieg oblicze\IeC {\'n}}{11}
\contentsline {subsection}{\numberline {2.2.3}Zastosowania}{12}
\contentsline {section}{\numberline {2.3}Ewolucyjny system wieloagentowy}{13}
\contentsline {subsection}{\numberline {2.3.1}Cel i budowa systemu}{13}
\contentsline {subsection}{\numberline {2.3.2}Mo\IeC {\.z}liwe dzia\IeC {\l }ania agent\IeC {\'o}w}{13}
\contentsline {section}{\numberline {2.4}Podsumowanie}{16}
\contentsline {chapter}{\numberline {3}Podej\IeC {\'s}cie immunologiczne w EMAS}{19}
\contentsline {section}{\numberline {3.1}Inspiracje biologiczne}{19}
\contentsline {section}{\numberline {3.2}Algorytmy immunologiczne}{21}
\contentsline {subsection}{\numberline {3.2.1}Selekcja klonalna}{22}
\contentsline {subsection}{\numberline {3.2.2}Selekcja negatywna}{23}
\contentsline {subsection}{\numberline {3.2.3}Sieci immunologiczne}{24}
\contentsline {section}{\numberline {3.3}Immunologiczny system wieloagentowy}{25}
\contentsline {subsection}{\numberline {3.3.1}Cel i budowa systemu}{25}
\contentsline {subsection}{\numberline {3.3.2}Metoda dzia\IeC {\l }ania}{27}
\contentsline {chapter}{\numberline {4}Wybrane aspekty implementacyjne}{29}
\contentsline {section}{\numberline {4.1}Ewolucyjny system wieloagentowy}{29}
\contentsline {section}{\numberline {4.2}Immunologiczny, ewolucyjny system wieloagentowy}{31}
\contentsline {subsection}{\numberline {4.2.1}Operator}{33}
\contentsline {subsection}{\numberline {4.2.2}Odleg\IeC {\l }o\IeC {\'s}\IeC {\'c} Mahalanobisa}{33}
\contentsline {subsection}{\numberline {4.2.3}Zbiornik retencyjny}{33}
\contentsline {subsection}{\numberline {4.2.4}Moment inercji}{34}
\contentsline {subsection}{\numberline {4.2.5}Modu\IeC {\l } statystyk}{34}
\contentsline {section}{\numberline {4.3}Uruchomienie oblicze\IeC {\'n} na platformie}{35}
\contentsline {subsection}{\numberline {4.3.1}Konfiguracja}{35}
\contentsline {chapter}{\numberline {5}Wyniki eksperyment\IeC {\'o}w obliczeniowych}{39}
\contentsline {section}{\numberline {5.1}Funkcje testowe}{39}
\contentsline {subsection}{\numberline {5.1.1}Funkcja De Jonga}{40}
\contentsline {subsection}{\numberline {5.1.2}Funkcja Rastrigina}{41}
\contentsline {subsection}{\numberline {5.1.3}Funkcja Schwefela}{42}
\contentsline {subsection}{\numberline {5.1.4}Funkcja Michalewicza}{43}
\contentsline {section}{\numberline {5.2}Miary wydajno\IeC {\'s}ci}{44}
\contentsline {subsection}{\numberline {5.2.1}Minimalna warto\IeC {\'s}\IeC {\'c} fitness}{44}
\contentsline {subsection}{\numberline {5.2.2}Liczba wywo\IeC {\l }a\IeC {\'n} funkcji fitness}{44}
\contentsline {subsection}{\numberline {5.2.3}Odchylenie standardowe}{44}
\contentsline {subsection}{\numberline {5.2.4}R\IeC {\'o}\IeC {\.z}norodno\IeC {\'s}\IeC {\'c} populacji}{44}
\contentsline {section}{\numberline {5.3}Wyb\IeC {\'o}r warto\IeC {\'s}ci parametr\IeC {\'o}w}{45}
\contentsline {subsection}{\numberline {5.3.1}Wp\IeC {\l }yw parametr\IeC {\'o}w na rezultaty}{46}
\contentsline {section}{\numberline {5.4}Weryfikacja hipotez}{50}
\contentsline {chapter}{\numberline {6}Podsumowanie}{57}
