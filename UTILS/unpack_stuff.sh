rm *.txt
N=27

mkdir charts

for i in `seq 1 $N`;
do
	mkdir simulation_$i
	tar -xf simulation_$i\.tar.gz -C simulation_$i

	cp simulation_$i/simulation/output.png charts/$i\_output.png
	cp simulation_$i/simulation/outputlog.png charts/$i\_outputlog.png
	cp simulation_$i/simulation/fitness_calls.png charts/$i\_fitness_calls.png
done

