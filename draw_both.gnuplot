#!/usr/bin/gnuplot

reset

set terminal pngcairo size 4000,500 enhanced font 'Verdana, 10' 
set output 'output.png'

set border linewidth 1 
set style line 1 lc rgb '#0060ad' lt 1 lw 2 pt 7
set style line 2 lc rgb 'red' lt 1 lw 2 pt 7

unset key

set xlabel 'iterations'
set ylabel 'fitness'

set xrange[-2:1002]
set yrange[0.1:]
set tics scale 1


set label 1 'emas' right at graph 0.9, graph 0.9 font 'Verdana, 20' textcolor ls 1
set label 2 'iemas' right at graph 0.9, graph 0.8 font 'Verdana, 20' textcolor ls 2

# ALL NORMAL
plot 'data_gnuplot_emas' using 1:2:3 w yerrorbars ls 1, '' using 1:2 w lines ls 1, 'data_gnuplot_iemas' using 1:2:3 w yerrorbars ls 2, '' using 1:2 w lines ls 2
#plot 'data_gnuplot_iemas' using 1:2:3 w yerrorbars ls 2, '' using 1:2 w lines ls 2

set logscale y
set output 'outputlog.png'

# LOGARITHM WITHOUT ERROR BARS
plot 'data_gnuplot_emas' using 1:2 w lines ls 1, 'data_gnuplot_iemas' using 1:2 w lines ls 2
#plot 'data_gnuplot_iemas' using 1:2 w lines ls 2

# FITNESS CALLS
unset logscale y
set ylabel 'fitness calls'
set output 'fitness_calls.png'
plot 'data_gnuplot_emas' using 1:8 w lines ls 1, 'data_gnuplot_iemas' using 1:8 w lines ls 2
#plot 'data_gnuplot_iemas' using 1:8 w lines ls 2

# TIME ELAPSED
set ylabel 'time elapsed'
set output 'time_elapsed.png'
plot 'data_gnuplot_emas' using 1:12 w lines ls 1, 'data_gnuplot_iemas' using 1:12 w lines ls 2
#plot 'data_gnuplot_iemas' using 1:12 w lines ls 2

# FITNESS CALLS PER TIME
set ylabel 'fitness calls per time'
set output 'fitness_calls_per_time.png'
plot 'data_gnuplot_emas' using 1:13 w lines ls 1, 'data_gnuplot_iemas' using 1:13 w lines ls 2
#plot 'data_gnuplot_iemas' using 1:13 w lines ls 2

# MOMENT OF INERTIA
set ylabel 'moment of inertia'
set output 'moment_of_inertia.png'
plot 'data_gnuplot_emas' using 1:14 w lines ls 1, 'data_gnuplot_iemas' using 1:14 w lines ls 2
#plot 'data_gnuplot_iemas' using 1:14 w lines ls 2

set ylabel 'moment of inertia'
set logscale y
set output 'moment_of_inertia_log.png'
plot 'data_gnuplot_emas' using 1:14 w lines ls 1, 'data_gnuplot_iemas' using 1:14 w lines ls 2
#plot 'data_gnuplot_iemas' using 1:14 w lines ls 2
unset logscale y

# NUMBER OF AGENTS
unset label 1
unset label 2

set ylabel 'number of agents'
set output 'number_of_agents.png'
set label 1 'agents_emas' at 50,37 font 'Verdana, 20' textcolor ls 1
set label 3 'agents_iemas' at 50,50 font 'Verdana, 20' textcolor ls 3
set label 4 'vampires_iemas' at 50,63 font 'Verdana, 20' textcolor ls 4
plot 'data_gnuplot_emas' using 1:4 w lines ls 1, 'data_gnuplot_iemas' using 1:4 w lines ls 3, 'data_gnuplot_iemas' using 1:5 w lines ls 4
#plot 'data_gnuplot_iemas' using 1:4 w lines ls 3, 'data_gnuplot_iemas' using 1:5 w lines ls 4



