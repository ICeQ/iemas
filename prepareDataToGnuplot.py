import sys

fitness = []
fitness_min = []
fitness_max = []
agents = []
vampires = []
fitness_calls = []
fitness_calls_min = []
fitness_calls_max = []
time_elapsed = []
moment_of_inertia = []
fitness_calls_per_time = []

howManyTries = 7 

if len(sys.argv) == 2:
	howManyTries = int(sys.argv[1])

howManyIterations = 1000

def parseFile(filename):
	j = 0
	for line in open(filename, 'r'):
		fields = line.split('\t')
		fitness[j] += float(fields[1])
		agents[j] += int(fields[2])
		vampires[j] += int(fields[3])
		fitness_calls[j] += int(fields[4])
		time_elapsed[j] += float(fields[7])
		moment_of_inertia[j] += float(fields[8])
		if time_elapsed[j] != 0:
			fitness_calls_per_time[j] += float(fitness_calls[j])/time_elapsed[j]
		
		if (float(fields[1]) < fitness_min[j]): fitness_min[j] = float(fields[1])
		if (float(fields[1]) > fitness_max[j]): fitness_max[j] = float(fields[1])
		if (int(fields[4]) < fitness_calls_min[j]): fitness_calls_min[j] = int(fields[4])
		if (int(fields[4]) > fitness_calls_max[j]): fitness_calls_max[j] = int(fields[4])

		j += 1

prefix = "Output/sim_"

for i in xrange(howManyIterations):
	fitness.append(0.0)
	fitness_min.append(100000.0)
	fitness_max.append(0.0)
	fitness_calls.append(0)
	fitness_calls_min.append(100000)
	fitness_calls_max.append(0)
	time_elapsed.append(0)
	agents.append(0)
	vampires.append(0)
	moment_of_inertia.append(0)
	fitness_calls_per_time.append(0)

for i in xrange(howManyTries):
	filename = prefix + str(i+1)
	parseFile(filename)

if howManyTries == 0:
	howManyTries = 1

for i in xrange(howManyIterations):
	fitness[i] /= howManyTries
	difference = fitness_max[i] - fitness_min[i]
	agents[i] /= howManyTries
	vampires[i] /= howManyTries
	fitness_calls[i] /= howManyTries
	calls_difference = fitness_calls_max[i] - fitness_calls_min[i]
	print str(i) + "\t" + str(fitness[i]) + "\t" + str(difference) + "\t" + str(agents[i]) + "\t" + str(vampires[i]) + "\t" + str(fitness_min[i]) + "\t" + str(fitness_max[i]) + "\t" + str(fitness_calls[i]) + "\t" + str(fitness_calls_min[i]) + "\t" + str(fitness_calls_max[i]) + "\t" + str(calls_difference) + "\t" + str(time_elapsed[i]) + "\t" + str(fitness_calls_per_time[i]) + "\t" + str(moment_of_inertia[i])

