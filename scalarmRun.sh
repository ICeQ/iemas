#!/bin/bash

# Envs:
#
# AmountOfIterations 
# RM
# NE
# TE
# ITS
# BT
# MHL
# IM
# GAE
# EV_MET

BIN_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
if [ -z "$PYTHONPATH" ]; then
	PYTHONPATH=$BIN_DIR
else
	PYTHONPATH=$PYTHONPATH:$BIN_DIR
fi

mkdir simulation
mkdir simulation/emas
mkdir simulation/emas/logs
mkdir simulation/iemas
mkdir simulation/iemas/logs

touch simulation/parameters
echo -e 'reproduction_minimum: '$RM'\nnewborn_energy: '$NE'\ntransferred_energy: '$TE'\nimmunological_time_span: '$ITS'\nbite_transfer: '$BT'\nmahalanobis: '$MHL'\nimmunological_maturity: '$IM'\ngood_agent_energy: '$GAE'\nevaluation_method: '$EV_MET > simulation/parameters

mkdir Output
for i in `seq 0 $AmountOfIterations`;
do
	python -m pyage.core.bootstrap femas_single_iemas 1 0 $RM 9999 $NE $TE $ITS $BT $MHL $IM $GAE $EV_MET
	FILE1=$(find . -maxdepth 1 -name "*.txt" -not -name "_stdout.txt")
	mv $FILE1 Output/sim_$i
done

python $BIN_DIR/prepareDataToGnuplot.py $AmountOfIterations > data_gnuplot_iemas

mv *.log simulation/iemas/logs
mv Output simulation/iemas/

# -------

#mkdir Output
#for i in `seq 1 $AmountOfIterations`;
#do
#	python -m pyage.core.bootstrap femas_single_emas 1 0 $RM 9999 $NE $TE $EV_MET
#	FILE2=$(find . -maxdepth 1 -name "*.txt" -not -name "_stdout.txt")
#	mv $FILE2 Output/sim_$i
#done

#python $BIN_DIR/prepareDataToGnuplot.py $AmountOfIterations > data_gnuplot_emas
gnuplot $BIN_DIR/draw.gnuplot

mv *.log simulation/emas/logs
mv Output simulation/emas/

mv data_gnuplot_emas simulation
mv data_gnuplot_iemas simulation
mv *.png simulation/


rm -f *.pyc
